// karma.conf.js
module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
    	"angular-1.5.3/angular.js",
    	"node_modules/angular-mocks/angular-mocks.js",
    	"module.js",
    	"movie-list.component.js",
    	"movie-rating.component.js",
    	"movie-list.spec.js"
    	//"**/*.js"

    ],
    exclude: [
    ],
    preprocessors: {

    },
    reporters: ["progress"],
    port: 9876,
    colors: true,
    loglevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["Chrome"],
    //browsers: ["karma-chrome-launcher"],
    singleRun: false,
    concurrency: Infinity
  });
};