(function (){
	"use strict";

var module = angular.module("psMovies");

module.component("movieApp", {
	templateUrl: "movie-app.component.html",
	//routing and navigation rules for this component
	$routeConfig: [
		{path: "/list", component: "movieList", name: "List" } , //when you see "list", load a component by name, name = friendly name
		{path: "/about", component: "appAbout", name: "About" },
		{path: "/detail/:id/...", component: "movieDetails", name: "Details" },
		{path: "/**", redirectTo: ["List"] }
	]
});




}());