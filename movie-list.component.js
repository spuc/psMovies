(function (){
	"use strict";

var module = angular.module("psMovies");

function fetchMovies($http)
{
	return $http.get("movies.json")
		.then(function(response) {
			return response.data;
		});
}

function controller($http)  {
			var model = this;
			model.movies = [];
			/*model.message = "hello from a component controller!"	;
		
			model.changeMessage = function () {
				model.message = "new message";
			}*/

			model.$onInit = function() {
				fetchMovies($http).then(function(movies) {
					model.movies = movies;
				})
			};

			model.setRating = function(movie, newRating) {
				movie.rating = newRating;  //change will propagate into child component where input binding is
			};

			model.upRating = function(movie) {
				if(movie.rating < 5 )
				{
					movie.rating += 1;
				}
			};

			model.goTo = function(id) {
				model.$router.navigate(["Details", {id: id}, "Overview"])  //start with Overview
			};

			model.downRating = function(movie) {
				if(movie.rating > 1 )
				{
					movie.rating -= 1;
				}
			};


		}

module.component("movieList", {
		//template: "hello from a component!"
		templateUrl: "movie-list.component.html",
		controllerAs: "model",
		controller: ["$http", controller] , //annotations
		bindings: {
			// I want routes associated with my component only
			"$router" : "<"  //gives access to my router when I'm inside my controller
		}

});

}());